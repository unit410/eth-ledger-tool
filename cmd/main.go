package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/unit410/eth-ledger-tool/pkg/derive"
)

var (
	flagAccount    = flag.Int("account", -1, "Derive an address at the speficied hardened account m/44'/60'/<account>/0/0")
	flagHardened   = flag.Bool("hardened", false, "Harden the account")
	flagMaxAccount = flag.Bool("max-account", false, "Derive an address at the maximum allowable path of m/44'/60'/<2^31-1>'/0/0")
	flagHelp       = flag.Bool("help", false, "Show help text")
)

func usage() {
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	flag.Parse()

	if *flagAccount >= 0 && *flagMaxAccount || *flagAccount < 0 && !*flagMaxAccount {
		fmt.Println("Error: Exactly one of --account and --max-account must be set")
		usage()
		os.Exit(1)
	}
	if *flagHelp {
		usage()
		os.Exit(0)
	}

	var accountIndex uint32
	// Get the base account
	if *flagAccount >= 0 {
		accountIndex = uint32(*flagAccount)
	} else if *flagMaxAccount {
		accountIndex = derive.PathMax
	}
	// Optionally harden
	if *flagHardened {
		accountIndex += derive.PathHardenedBase
	}

	fmt.Printf("Path: %s\n", derive.HumanDerivationPath(accountIndex))
	fmt.Printf("Address: %s\n", derive.DeriveLedgerAddress(accountIndex))
}

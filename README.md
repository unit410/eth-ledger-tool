Ledger Eth Address Derivation
=============================

Simple tool to extract an Ethereum addresses from a connected ledger.

**Usage**

```shell
make build

./eth-ledger-tool --account 0
> Path: m/44'/60'/0/0/0
> Address:  0xABCDEF1234567890ABCDEF1234567890ABCDEF12

./eth-ledger-tool --account 5 --hardened
> Path: m/44'/60'/5'/0/0
> Address:  0x1234567890ABCDEF1234567890ABCDEF12ABCDEF
```